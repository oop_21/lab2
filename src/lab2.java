import java.util.Scanner;

public class lab2 {
    public static void main(String[] args) {
        Scanner Sc = new Scanner(System.in);
        System.out.print("Your Grade : ");
        int grade = Sc.nextInt();

        if (grade >= 80) {
            System.out.println("A");
        } else if (grade < 80 && grade >= 75){
            System.out.println("B+");
        } else if (grade < 75 && grade >= 70){
            System.out.println("B");
        } else if (grade < 70 && grade >= 65){
            System.out.println("C+");
        }else if (grade < 65 && grade >= 60){
            System.out.println("C");
        } else if (grade < 60 && grade >= 55){
            System.out.println("D+");
        } else if (grade < 55 && grade >= 50){
            System.out.println("D");
        } else {
            System.out.println("F");
        }
    }
}
