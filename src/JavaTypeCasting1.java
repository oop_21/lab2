public class JavaTypeCasting1 {
    public static void main(String[] args) {
        int myInt = 9 ;
        double myDouble = myInt ; // Autimatic casting : int ti double
        System.out.println(myInt); // Outputs 9
        System.out.println(myDouble); // Outputs 9.0
    }
}
